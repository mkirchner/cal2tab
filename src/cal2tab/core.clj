(ns cal2tab.core
  (:use [clojure.tools.cli :only [cli]])
  (:require
    [clj-time.core :as tc]
    [clj-time.format :as tf]
    [clj-time.coerce :as tcr]
    [clojure.data.json :as json]
    [clojure.data.csv :as csv]
    [clojure.java.io :as io])
  (:gen-class))


(def timefmt (tf/formatter "yyyy-MM-dd'T'HH:mm:ssZ"))

(defn start 
  [e] 
  (tcr/to-long (tf/parse timefmt (-> e :start :dateTime))))

(defn end 
  [e] 
  (tcr/to-long (tf/parse timefmt (-> e :end :dateTime))))

(defn as-interval-map
  [e]
  {
   :meta [(select-keys e [:summary :description])]
   :start (start e)
   :end (end e)})

(defn sort-events
  "Sorts events by start date."
  [events]
  (sort-by #(:start %) events))

(defn collate-events 
  "If two events overlap, collates the events or returns nil
   if the events are independent."
  [e1 e2]
  (if (< (:start e2) (:end e1))
    ; merge events
    { :meta (concat (:meta e1) (:meta e2))
      :start (:start e1)
      :end (max (:end e1) (:end e2)) }
    nil))

(defn collate-all
  "Collates all events w/ overlapping date intervals.
   Returns a list of collated events."
  [events]
  (loop [e1        (first events)
         es       (rest events)
         collated []]
    (if-let [e2 (first es)]
      (if-let [c (collate-events e1 e2)]
        (recur c (rest es) collated)
        (recur e2 (rest es) (cons e1 collated)))
      (reverse (cons e1 collated)))))

(defn to-csv
  [tzid e]
  (let [start (tcr/from-long (:start e))
        end   (tcr/from-long (:end e))
        duration-in-mins (quot (- (:end e) (:start e)) 60000)
        duration-hrs  (quot duration-in-mins 60)
        duration-mins (rem duration-in-mins 60)
        text  (clojure.string/join " / " (map #(:summary %) (:meta e)))
        tz    (tc/time-zone-for-id tzid)]
    [(tf/unparse (tf/formatter "yyyy-MM-dd" tz) start)
     (tf/unparse (tf/formatter "HH:mm" tz) start)
     (tf/unparse (tf/formatter "yyyy-MM-dd" tz) end)
     (tf/unparse (tf/formatter "HH:mm" tz) end)
     (str duration-hrs ":" duration-mins)
     text]))

(defn convert
  [jsonstr]
  (map (partial to-csv "Europe/Berlin")
    (collate-all
      (sort-events
        (map as-interval-map 
           (:items (json/read-str jsonstr :key-fn keyword)))))))


(defn -main
  "Convert Goolge Calendar JSON exports to tabular data,
   merging overlapping events in the process."
  [& cmdlineargs]
  ;; work around dangerous default behaviour in Clojure
  (alter-var-root #'*read-eval* (constantly false))

  (let [[options args banner] (cli cmdlineargs
                                   "cal2tab -- Generate DAV invoices from Google Calendar JSON data"
                                   ["-h" "--help" "Show help" :default false :flag true]
                                   ["-i" "--json" "The JSON input file"]
                                   ["-o" "--csv" "The CSV output file"])]
    ; check the command line args
    (when (:help options)
      (println banner)
      (System/exit 0))
    ; process the file
    (with-open [out-file (io/writer (:csv options))]
      (csv/write-csv out-file
        (convert (slurp (:json options)))))))

