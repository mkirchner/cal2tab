(defproject cal2tab "0.1.0"
  :description "Converts Google Calendar JSON lists of events to CSV"
  :url "https://bitbucket.org/mkirchner/cal2tab"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [clj-time "0.6.0"]
                 [org.clojure/data.csv "0.1.2"]
                 [org.clojure/data.json "0.2.3"]
                 [org.clojure/tools.cli "0.3.0"]]
  :main cal2tab.core)
