# cal2tab

Quick hack that converts Google Calendar JSON event lists to CSV.

## Usage

    $ java -jar cal2tab-0.1.0.jar -i infile.json -o outfile.csv

### Bugs

Probably.


## License

Copyright © 2014 Marc Kirchner

Distributed under the Eclipse Public License, the same as Clojure.
